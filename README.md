# Labo de contribution itinérant

## Faire partie d'un labo de contribution itinérant

Il s'agit d'un groupe, alors il est possible d'entrer dans un groupe existant
ou d'en créer en nouveau. Tous les contenus de ce répo sont libres donc il vous
suffit de forker ce répo pour créer un nouveau labo !

Un premier groupe : open-scientist.org/labo-itinérant

## Ingrédients

### Contrbution

Tout logiciel, projet, jeu de données, objet physique pour lequel il y a une
invitation ou une absence de barrière artificielle à la contribution peut être
l'occasion pour ses utilisateurs de contribuer.

Les logiciels libres invitent à la contribution et sont un bon point d'entrée.

### S'entrainer en contribuant - apprentissage pair à pair

Les compétences s'aquierent en les rencontrant, en apprenant à exercer un
art ou en observant quelqu'un·e d'autre les exercer.

Venez participer à un labo de contribution ambulant, pour permettre à d'autres
d'aquérir les savoir-faires que vous avez et pour vous entrainer sur les 
savoirs-faires d'autres personnes.

Dès lors qu'un savoir-faire manque pour répondre à un problème, une ou des personnes
extérieures qui peuvent l'avoir sont sollicitées.

### Nomadisme

Pour atteindre les gens là où ils et elles sont et pour permettre à toute personne
de contribuer, sans barrière physique, le labo se déplace au grés des besoins.


## Sessions avec le labo ambulant de contribution

[Prenez rendez-vous !](mailto:user701@orange.fr)

[Sessions d'entrainement en pair-à-pair, Strasbourg](https://gitlab.com/jibe-b/forum-ouvert-entrainement-dev)